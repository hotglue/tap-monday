"""Stream type classes for tap-monday."""

import copy
from typing import Any, List, Optional, Dict, Iterable

import requests
from datetime import datetime
from singer_sdk import typing as th
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from datetime import datetime, timedelta
from singer_sdk.helpers._state import (
    finalize_state_progress_markers,
    log_sort_error,
)
from singer_sdk.helpers.jsonpath import extract_jsonpath
from tap_monday.client import MondayStream
from singer_sdk.exceptions import InvalidStreamSortException


class WorkspacesStream(MondayStream):
    name = "workspaces"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("kind", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("state", th.StringType),
    ).to_dict()

    @property
    def query(self) -> str:
        return """
            query {
              boards {
                workspace {
                  id
                  created_at
                  state
                  name
                  kind
                  description
                }
              }
            }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"]:
            yield row["workspace"]


class BoardsStream(MondayStream):
    name = "boards"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("app_feature_id", th.StringType),
        th.Property("board_folder_id", th.StringType),
        th.Property("board_kind" , th.StringType),
        th.Property("communication", th.StringType),
        th.Property("description", th.StringType),
        th.Property("item_terminology", th.StringType),
        th.Property("items_count", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("parent_object_id", th.NumberType),
        th.Property("permissions", th.StringType),
        th.Property("pos", th.StringType),
        th.Property("state", th.StringType),
        th.Property( "system_entity_name", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("workspace_id", th.StringType),
        #@TODO: uncomment if columns are required in the base query
        # th.Property("columns", th.ArrayType(th.ObjectType(
        #     th.Property("app", th.StringType),
        #     th.Property("archived", th.BooleanType),
        #     th.Property("column_model_id", th.StringType),
        #     th.Property("description", th.StringType),
        #     th.Property("entity_type", th.StringType),
        #     th.Property("id", th.StringType),
        #     th.Property("pos", th.StringType),
        #     th.Property("settings_str", th.StringType),
        #     th.Property("title", th.StringType),
        #     th.Property("type", th.StringType),
        #     th.Property("width", th.NumberType),
        # ))),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params = {
            "page": next_page_token or 1,
            "board_limit": self.config["board_limit"]
        }
        return params


    @property
    def query(self) -> str:
        #@TODO: add selected properties query support
        return """
            query ($page: Int!, $board_limit: Int!) {
                boards(limit: $board_limit, page: $page, order_by: created_at) {
                        id
                        app_feature_id
                        board_folder_id
                        board_kind
                        communication
                        description
                        item_terminology
                        items_count
                        name
                        parent_object_id
                        permissions
                        state
                        system_entity_name
                        updated_at
                        workspace_id
                    }
                }
        """

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "board_id": record["id"],
        }

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"]:
            yield row

    def post_process(self, row: dict, context: Optional[dict] = None) -> dict:
        return row

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:
        current_page = previous_token if previous_token is not None else 1
        prev_data = len(response.json()["data"][self.name])
        board_limit = self.config["board_limit"]
        if prev_data == board_limit:
            next_page_token = current_page + 1
        else:
            next_page_token = None
        return next_page_token


class BoardViewsStream(MondayStream):
    name = "board_views"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardsStream
    ignore_parent_replication_keys = True
    # records_jsonpath: str = "$.data.boards[0].groups[*]"  # TODO: use records_jsonpath instead of overriding parse_response
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("settings_str", th.StringType),
        th.Property("type", th.StringType),
        th.Property("view_specific_data_str", th.StringType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        return {
            "board_id": context["board_id"]
        }

    @property
    def query(self) -> str:
        return """
            query ($board_id: [ID!]) {
                boards(ids: $board_id) {
                    views {
                        id
                        name
                        settings_str
                        type
                        view_specific_data_str
                    }
                }
            }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"][0]["views"]:
            yield row


class ActivityLogStream(MondayStream):
    name = "activity_log"
    primary_keys = ["id"]
    replication_key = "created_at"
    parent_stream_type = BoardsStream
    ignore_parent_replication_keys = True
    # records_jsonpath: str = "$.data.boards[0].groups[*]"  # TODO: use records_jsonpath instead of overriding parse_response
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("event", th.StringType),
        th.Property("data", th.StringType),
        th.Property("created_at", th.DateTimeType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params = {
            "board_id": context["board_id"]
        }
        if self.replication_key:
            start_date = self.get_starting_time(context) + timedelta(seconds=1)
            if start_date:
                params["created_at"] = start_date.isoformat()

        return params

    @property
    def query(self) -> str:
        return """
            query ($board_id: [ID!], $created_at: ISO8601DateTime) {
                boards(ids: $board_id) {
                    activity_logs (from: $created_at) {
                        id
                        event
                        data
                        created_at
                    }
                }
            }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        activity_logs = resp_json["data"]["boards"][0].get("activity_logs") or []
        for row in activity_logs:
            row["created_at"] = int(row["created_at"]) / 10000000
            row["created_at"] = datetime.fromtimestamp(row["created_at"]).isoformat()
            yield row


class GroupsStream(MondayStream):
    name = "groups"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardsStream
    ignore_parent_replication_keys = True
    # records_jsonpath: str = "$.data.boards[0].groups[*]"  # TODO: use records_jsonpath instead of overriding parse_response
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("title", th.StringType),
        th.Property("position", th.NumberType),
        th.Property("board_id", th.StringType),
        th.Property("color", th.StringType),
        th.Property("deleted", th.BooleanType),
        th.Property("archived", th.BooleanType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        return {
            "board_id": context["board_id"]
        }

    @property
    def query(self) -> str:
        return """
            query ($board_id: [ID!]) {
                boards(ids: $board_id) {
                    groups {
                        title
                        position
                        id
                        color
                        deleted
                        archived
                    }
                }
            }
        """
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "board_id": context["board_id"],
            "group_id": record["id"],

        }
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"][0]["groups"]:
            yield row

    def post_process(self, row: dict, context: Optional[dict] = None) -> dict:
        row["position"] = float(row["position"])
        row["board_id"] = context["board_id"]
        return row


class ColumnsStream(MondayStream):
    name = "columns"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardsStream
    ignore_parent_replication_keys = True
    # records_jsonpath: str = "$.data.boards[0].groups[*]"  # TODO: use records_jsonpath instead of overriding parse_response
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("archived", th.BooleanType),
        th.Property("app", th.StringType),
        th.Property("settings_str", th.StringType),
        th.Property("title", th.StringType),
        th.Property("description", th.StringType),
        th.Property("type", th.StringType),
        th.Property("width", th.IntegerType),
        th.Property("board_id", th.StringType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        return {
            "board_id": context["board_id"],
        }

    @property
    def query(self) -> str:
        return """
            query ($board_id: [ID!]) {
                boards(ids: $board_id) {
                    columns {
                        app
                        archived
                        id
                        settings_str
                        title
                        description
                        type
                        width
                    }
                }
            }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"]:
            for column in row["columns"]:
                yield column

    def post_process(self, row: dict, context: Optional[dict] = None) -> dict:
        row["board_id"] = context["board_id"]
        return row


class TeamsStream(MondayStream):
    name = "teams"
    primary_keys = ["id"]
    replication_key = None
    ignore_parent_replication_keys = True
    schema = th.PropertiesList(
        th.Property("name", th.StringType),
        th.Property("picture_url", th.BooleanType),
        th.Property("users", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

    @property
    def query(self) -> str:
        return """
            query {
                teams {
                    id
                    name
                    picture_url
                    users {
                        created_at
                        phone
                    }
                }
            }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["teams"]:
            yield row


class UsersStream(MondayStream):
    name = "users"
    primary_keys = ["id"]
    replication_key = None
    ignore_parent_replication_keys = True
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("email", th.StringType),
        th.Property("name", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("account", th.CustomType({"type": ["array", "object"]})),
    ).to_dict()

    @property
    def query(self) -> str:
        return """
        query {
            users (limit: 50) {
                id
                name
                email
                created_at
                account {
                    name
                }
            }
        }
        """

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["users"]:
            yield row

class BoardItemsStream(MondayStream):
    name = "board_items"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardsStream
    ignore_parent_replication_keys = True
    cursor = None
    next_page_token_jsonpath = "$.data.boards[:1][items_page].cursor"
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {
            "item_id": record["id"],
            "board_id": context.get("board_id"),
        }

    schema = th.PropertiesList(

        th.Property("id", th.StringType),
        th.Property("account_id", th.StringType),
        th.Property("column_values_str", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("creator_id", th.StringType),
        th.Property("email", th.StringType),
        th.Property("name", th.StringType),
        th.Property("relative_link", th.StringType),
        th.Property("state", th.StringType),
        th.Property("updated_at", th.DateTimeType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        self.cursor = next_page_token
        params = {
            "board_id": context["board_id"],
            "board_limit": self.config["board_limit"]
        }
        if next_page_token:
            params["page"] = next_page_token
        return params    

    @property
    def query(self) -> str:
        base_query = """
            query ($board_id: [ID!], $page: String!, $board_limit: Int!) {
                boards(ids: $board_id) {
                    items_page(
                        limit: $board_limit
                        %s 
                    ) {
                        cursor
                        items {
                            account_id
                            column_values_str
                            created_at
                            creator_id
                            email
                            id
                            name
                            relative_link
                            state
                            updated_at
                        }
                    }
                }
            }
        """
        cursor_field = "cursor: $page" if self.cursor is not None else ""
        query = base_query % cursor_field
        if not self.cursor:
            query = query.replace("$page: String!,", "")
        return query
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"][0]["items_page"]['items']:
            yield row
    def _sync_records(  # noqa C901  # too complex
        self, context: Optional[dict] = None
    ) -> None:
        """Sync records, emitting RECORD and STATE messages.

        Args:
            context: Stream partition or context dictionary.

        Raises:
            InvalidStreamSortException: TODO
        """
        record_count = 0
        current_context: Optional[dict]
        context_list: Optional[List[dict]]
        context_list = [context] if context is not None else self.partitions
        selected = self.selected

        for current_context in context_list or [{}]:
            partition_record_count = 0
            current_context = current_context or None
            state = self.get_context_state(current_context)
            state_partition_context = self._get_state_partition_context(current_context)
            self._write_starting_replication_value(current_context)
            child_context: Optional[dict] = (
                None if current_context is None else copy.copy(current_context)
            )
            for record_result in self.get_records(current_context):
                if isinstance(record_result, tuple):
                    # Tuple items should be the record and the child context
                    record, child_context = record_result
                else:
                    record = record_result
                child_context = copy.copy(
                    self.get_child_context(record=record, context=child_context)
                )
                for key, val in (state_partition_context or {}).items():
                    # Add state context to records if not already present
                    if key not in record:
                        self.board_item_ids.add(record["id"])
                        record[key] = val

                
                self._check_max_record_limit(record_count)
                if selected:
                    if (record_count - 1) % self.STATE_MSG_FREQUENCY == 0:
                        self._write_state_message()
                    self._write_record_message(record)
                    try:
                        self._increment_stream_state(record, context=current_context)
                    except InvalidStreamSortException as ex:
                        log_sort_error(
                            log_fn=self.logger.error,
                            ex=ex,
                            record_count=record_count + 1,
                            partition_record_count=partition_record_count + 1,
                            current_context=current_context,
                            state_partition_context=state_partition_context,
                            stream_name=self.name,
                        )
                        raise ex

                record_count += 1
                partition_record_count += 1
            #Sync children after fetching all board items
            self._sync_children(child_context)    
            if current_context == state_partition_context:
                # Finalize per-partition state only if 1:1 with context
                finalize_state_progress_markers(state)
        if not context:
            # Finalize total stream only if we have the full full context.
            # Otherwise will be finalized by tap at end of sync.
            finalize_state_progress_markers(self.stream_state)
        self._write_record_count_log(record_count=record_count, context=context)
        # Reset interim bookmarks before emitting final STATE message:
        self._write_state_message()        
class GroupItemsStream(MondayStream):
    name = "group_items"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = GroupsStream
    ignore_parent_replication_keys = True
    cursor = None
    next_page_token_jsonpath = "$.data.boards[:1].groups[:1].[items_page].cursor"
    schema = th.PropertiesList(
        
        th.Property("id", th.StringType),
        th.Property("account_id", th.StringType),
        th.Property("column_values_str", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("creator_id", th.StringType),
        th.Property("email", th.StringType),
        th.Property("name", th.StringType),
        th.Property("relative_link", th.StringType),
        th.Property("state", th.StringType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("group_id", th.StringType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        self.cursor = next_page_token
        params = {
            "board_id": context["board_id"],
            "group_id": context["group_id"],
            "board_limit": self.config["board_limit"]
        }
        if next_page_token:
            params["page"] = next_page_token
        return params    

    @property
    def query(self) -> str:
        base_query = """
            query ($board_id: [ID!], $page: String!, $board_limit: Int!, $group_id: [String!]) {
                boards(ids: $board_id) {
                    groups(ids: $group_id) {
                        items_page(
                            limit: $board_limit
                            %s 
                        ) {
                            cursor
                            items {
                                account_id
                                column_values_str
                                created_at
                                creator_id
                                email
                                id
                                name
                                relative_link
                                state
                                updated_at
                            }
                        }
                    }
                }
            }
        """
        cursor_field = "cursor: $page" if self.cursor is not None else ""
        query = base_query % cursor_field
        if not self.cursor:
            #we can only send valid value for cursor. So remove $page if no valid value for cursor is found
            query = query.replace("$page: String!,", "")
        return query
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]["boards"][0]['groups'][0]["items_page"]['items']:
            group_id = self.partitions[-1]['group_id']
            row['group_id'] = group_id
            yield row

class BoardItemMirrorsStream(MondayStream):
    #TODO we can fetch batch mirrors by providing list of item ids
    name = "board_items_mirrors"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardItemsStream
    ignore_parent_replication_keys = True
    records_jsonpath = "$.data.items[:1].column_values[*]"
    batch_size = 10
    
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("display_value", th.StringType),
        th.Property("text", th.StringType),
        th.Property("type", th.StringType),
        th.Property("value", th.StringType),
        th.Property("column", th.CustomType({"type": ["object", "string"]})),
        th.Property("item_id", th.StringType),
        th.Property("board_id", th.StringType),
    ).to_dict()

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        self.cursor = next_page_token
        params = {
            "item_id": context.get("item_ids")
        }
        if next_page_token:
            params["page"] = next_page_token
        else:
            params['page'] = 1    
        return params

    @property
    def query(self) -> str:
        base_query = """
            query ($item_id: [ID!], $page: Int!) {
                items(ids: $item_id, page: $page) {
                    column_values {
                        ... on MirrorValue {
                            display_value
                            id
                            text
                            type
                            value
                            column {
                                app
                                archived
                                column_model_id
                                description
                                entity_type
                                id
                                settings_str
                                title
                                type
                                width
                            }
                    }
        }
                }
            }
        """
        
        query = base_query
        return query
    
    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Any:
        current_page = previous_token if previous_token is not None else 1
        data = response.json()
        if "data" in data and "items" in data['data'] and len(data['data']['items'])>0:
            data = data["data"]["items"][0]["column_values"]
        else:
            #No data found
            return None    
        if len(data)>0:
            next_page_token = current_page + 1
        else:
            next_page_token = None
        return next_page_token
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())
    
    def post_process(self, row: dict, context: Optional[dict] = None) -> Optional[dict]:
        if len(row)>0:
            row['item_id'] = context.get('item_id')
            row["board_id"] = context.get("board_id")
            return row
        return None
    
    def _sync_records(  # noqa C901  # too complex
        self, context: Optional[dict] = None
    ) -> None:
        """Sync records, emitting RECORD and STATE messages.

        Args:
            context: Stream partition or context dictionary.

        Raises:
            InvalidStreamSortException: TODO
        """
        record_count = 0
        current_context: Optional[dict]
        context_list: Optional[List[dict]]
        context_list = [context] if context is not None else self.partitions
        selected = self.selected
        board_item_ids = list(self.board_item_ids)
        for i in range(0, len(board_item_ids), self.batch_size):
            partition_record_count = 0
            # current_context = current_context or None
            current_context = {"item_ids":board_item_ids[i:i+self.batch_size]}
            state = self.get_context_state(current_context)
            state_partition_context = self._get_state_partition_context(current_context)
            self._write_starting_replication_value(current_context)
            child_context: Optional[dict] = (
                None if current_context is None else copy.copy(current_context)
            )
            for record_result in self.get_records(current_context):
                if isinstance(record_result, tuple):
                    # Tuple items should be the record and the child context
                    record, child_context = record_result
                else:
                    record = record_result
                child_context = copy.copy(
                    self.get_child_context(record=record, context=child_context)
                )
                for key, val in (state_partition_context or {}).items():
                    # Add state context to records if not already present
                    if key not in record:
                        self.board_item_ids.add(record["id"])
                        record[key] = val

                
                self._check_max_record_limit(record_count)
                if selected:
                    if (record_count - 1) % self.STATE_MSG_FREQUENCY == 0:
                        self._write_state_message()
                    self._write_record_message(record)
                    try:
                        self._increment_stream_state(record, context=current_context)
                    except InvalidStreamSortException as ex:
                        log_sort_error(
                            log_fn=self.logger.error,
                            ex=ex,
                            record_count=record_count + 1,
                            partition_record_count=partition_record_count + 1,
                            current_context=current_context,
                            state_partition_context=state_partition_context,
                            stream_name=self.name,
                        )
                        raise ex

                record_count += 1
                partition_record_count += 1
            #Sync children after fetching all board items
            self._sync_children(child_context)    
            if current_context == state_partition_context:
                # Finalize per-partition state only if 1:1 with context
                finalize_state_progress_markers(state)
        if not context:
            # Finalize total stream only if we have the full full context.
            # Otherwise will be finalized by tap at end of sync.
            finalize_state_progress_markers(self.stream_state)
        self._write_record_count_log(record_count=record_count, context=context)
        # Reset interim bookmarks before emitting final STATE message:
        self._write_state_message()        
    
class BoardItemDropdownsStream(BoardItemMirrorsStream):
    #TODO we can fetch in batches by providing item ids
    name = "board_items_dropdowns"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardItemsStream
    ignore_parent_replication_keys = True
    
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("text", th.StringType),
        th.Property("type", th.StringType),
        th.Property("value", th.StringType),
        th.Property("column", th.CustomType({"type": ["object", "string"]})),
        th.Property("values", th.CustomType({"type": ["array", "string"]})),
        th.Property("item_id", th.StringType),
        th.Property("board_id", th.StringType),
    ).to_dict()  
    
    @property
    def query(self) -> str:
        base_query = """
            query ($item_id: [ID!], $page: Int!) {
                items(ids: $item_id, page: $page) {
                    column_values {
                        ... on DropdownValue {
                            id
                            text
                            type
                            value
                            column {
                                app
                                archived
                                column_model_id
                                description
                                entity_type
                                id
                                settings_str
                                title
                                type
                                width
                            }
                            values {
                                id
                                label
                            }
                        }
                    }
        
                }
            }
        """

        query = base_query
        return query
class BoardItemStatusStream(BoardItemMirrorsStream):
    # TODO we can fetch in batches by providing item ids
    name = "board_items_status"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BoardItemsStream
    ignore_parent_replication_keys = True
    
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("index", th.IntegerType),
        th.Property("is_done", th.BooleanType),
        th.Property("label", th.StringType),
        th.Property("text", th.StringType),
        th.Property("type", th.StringType),
        th.Property("value", th.StringType),
        th.Property("column", th.CustomType({"type": ["object", "string"]})),
        th.Property("item_id", th.StringType),
        th.Property("board_id", th.StringType),
    ).to_dict()  
    
    @property
    def query(self) -> str:
        base_query = """
            query ($item_id: [ID!], $page: Int!) {
                items(ids: $item_id, page: $page) {
                  column_values {
                    ... on StatusValue {
                        id
                        index
                        is_done
                        label
                        text
                        type
                        update_id
                        updated_at
                        value
                        column {
                            app
                            archived
                            column_model_id
                            description
                            entity_type
                            id
                            settings_str
                            title
                            type
                            width
                        }
                    }
                }
        
                }
            }
        """

        query = base_query
        return query
