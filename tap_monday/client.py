"""GraphQL client handling, including MondayStream base class."""
import copy
from optparse import Option

import backoff
import requests
from datetime import datetime
from dateutil.parser import parse
from requests.exceptions import ConnectionError

from typing import Any, Callable, Optional, Iterable

from singer_sdk.streams import GraphQLStream
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from backports.cached_property import cached_property
from urllib3.exceptions import ProtocolError, InvalidChunkLength
from requests.exceptions import JSONDecodeError

class MondayStream(GraphQLStream):
    """Monday stream class."""

    url_base = "https://api.monday.com/v2"
    board_item_ids = set()
    @property
    def http_headers(self) -> dict:
        headers = {}
        headers["Authorization"] = "Bearer {}".format(self.config.get("access_token"))
        headers["Content-Type"] = "application/json"
        headers["API-Version"] = "2023-10" #upgradeable to 2024-01, 2024-04

        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        resp_json = response.json()
        for row in resp_json["data"]:
            yield row

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures.

        Developers may override this method to provide custom backoff or retry
        handling.

        Args:
            func: Function to decorate.

        Returns:
            A decorated method.
        """
        
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (RetriableAPIError, requests.exceptions.ReadTimeout, ConnectionError, ConnectionResetError,ProtocolError,InvalidChunkLength,requests.RequestException,JSONDecodeError),
            max_tries=12,
            factor=5
        )(func)
        return decorator

    def calculate_sync_cost(
        self,
        request: requests.PreparedRequest,
        response: requests.Response,
        context: Optional[dict],
    ):
        """Return the cost of the last REST API call."""
        return {"rest": 1, "graphql": 0, "search": 0}

    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Request records from REST endpoint(s), returning response records.

        If pagination is detected, pages will be recursed automatically.

        Args:
            context: Stream partition or context dictionary.

        Yields:
            An item for every record in the response.

        Raises:
            RuntimeError: If a loop in pagination is detected. That is, when two
                consecutive pagination tokens are identical.
        """
        next_page_token: Any = None
        finished = False
        decorated_request = self.request_decorator(self._request)

        while not finished:
            prepared_request = self.prepare_request(
                context, next_page_token=next_page_token
            )

            resp = decorated_request(prepared_request, context)
            self.update_sync_costs(decorated_request, resp, context)

            for row in self.parse_response(resp):
                yield row
            previous_token = copy.deepcopy(next_page_token)
            next_page_token = self.get_next_page_token(
                response=resp, previous_token=previous_token
            )
            if next_page_token and next_page_token == previous_token:
                raise RuntimeError(
                    f"Loop detected in pagination. "
                    f"Pagination token {next_page_token} is identical to prior token."
                )
            # Cycle until get_next_page_token() no longer returns a value
            finished = not next_page_token

        self.log_sync_costs()

    def validate_response(self, response: requests.Response) -> None:
        try:
            resp_json = response.json()
        except JSONDecodeError:
            self.logger.warn(f"JSON decode error for URL:{response.request.url}: body:{response.request.body}" )
            raise RetriableAPIError('JSON Decode error')    
        if "data" not in resp_json and "error_message" in resp_json:
             raise RetriableAPIError(resp_json['error_message'])
        
        if response.status_code == 408:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            self.logger.warn(f"{response.status_code}: {response.reason}")
            raise RetriableAPIError(response.text)
        elif 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            self.logger.warn(f"{response.status_code}: {response.reason}")
            raise FatalAPIError(response.text)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            self.logger.warn(f"{response.status_code}: {response.reason}")
            raise RetriableAPIError(response.text)

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        start_date = parse(start_date) if start_date else datetime(1970, 1, 1, 0, 0)
        start_date = self.get_starting_timestamp(context) or start_date
        return start_date
    
    @cached_property
    def selected_properties(self):
        #TODO - improve this function to support nested properties
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)
        return selected_properties